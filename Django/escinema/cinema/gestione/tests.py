from django.urls import reverse
from django.test import TestCase, Client
from .models import *

# Create your tests here.

class VotaViewTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.film1 = Film.objects.create(
            titolo = 'film',
            immagine = 'foto.jpg',
            regista = 'regista',
            attore = 'attore',
            sinossi = 'sinossi',
            categoria = 'categoria',
            durata = '100min',
            media_valutazione = '0')

        self.user1 = User.objects.create(
            username = 'user1',
            email = 'user@prova.com'
        )
        self.user1.set_password('12345')
        self.user1.save()
        self.logged_in = self.client.login(username='user1',password='12345')

        self.vota_url = reverse('gestione:recensione',args =[self.film1.id])

    def test_votazione_corretta_POST_succes_redirect(self):
        '''
            test che si occupa di verificare che dato l'url delle recensioni effettua una richiesta
            POST con i dati relativi alla recensione di un film e che poi si venga redirectati alla
            home
        '''
        response = self.client.post(self.vota_url, {
            'film' : self.film1.id,
            'user' : self.user1.id,
            'recensione' : 'gradevole',
            'stelle' : '7'
        }, follow=True)

        self.assertRedirects(response,reverse('home'), status_code=302, target_status_code=200)
        self.assertTemplateUsed(response,'home.html')

    def test_votazione_non_corretta_recensione_mancante_POST_not_success_redirect(self):
        '''
            test che si occupa di verificare che dato l'url delle recensioni effettua una richiesta
            POST con il dato di testo (recensione) mancante e quindi non si viene redirectati e si 
            visualliza la stessa pagina con l'errore
        '''

        response = self.client.post(self.vota_url, {
            'film' : self.film1.id,
            'user' : self.user1.id,
            'stelle' : '7'
        }, follow=True)

        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response,'gestione/recensioni.html')

    def test_votazione_non_corretta_stelle_mancante_POST_not_success_redirect(self):
        '''
            test che si occupa di verificare che dato l'url delle recensioni effettua una richiesta
            POST con il dato numerico (stelle) mancante e quindi non si viene redirectati e si 
            visualliza la stessa pagina con l'errore
        '''

        response = self.client.post(self.vota_url, {
            'film' : self.film1.id,
            'user' : self.user1.id,
            'recensione' : 'gradevole'
        }, follow=True)

        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response,'gestione/recensioni.html')

    def test_votazione_stelle_maggiore_10_POST_not_success_redirect(self):
        '''
            test che si occupa di verificare che dato l'url delle recensioni effettua una richiesta
            POST con il dato numerico (stelle) maggiore di 10 quindi non si viene redirectati e si 
            visualliza la stessa pagina con l'errore
        '''

        response = self.client.post(self.vota_url, {
            'film' : self.film1.id,
            'user' : self.user1.id,
            'recensione' : 'gradevole',
            'stelle' : '15'
        }, follow=True)

        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response,'gestione/recensioni.html')

    def test_votazione_stelle_minore_1_POST_not_success_redirect(self):
        '''
            test che si occupa di verificare che dato l'url delle recensioni effettua una richiesta
            POST con il dato numerico (stelle) minore di 1 quindi non si viene redirectati e si 
            visualliza la stessa pagina con l'errore
        '''

        response = self.client.post(self.vota_url, {
            'film' : self.film1.id,
            'user' : self.user1.id,
            'recensione' : 'gradevole',
            'stelle' : '0'
        }, follow=True)

        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response,'gestione/recensioni.html')

    def test_votazione_stelle_carattere_invece_intero_POST_not_success_redirect(self):
        '''
            test che si occupa di verificare che dato l'url delle recensioni effettua una richiesta
            POST con il dato numerico stelle inserito come carattere quindi non si viene redirectati 
            e si visualliza la stessa pagina con l'errore
        '''

        response = self.client.post(self.vota_url, {
            'film' : self.film1.id,
            'user' : self.user1.id,
            'recensione' : 'gradevole',
            'stelle' : 'sette'
        }, follow=True)

        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response,'gestione/recensioni.html')