from django.contrib.auth.models import User
from django.forms import *
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.


class Giorno(models.Model):
    giorno = models.DateField()
    orario = models.TimeField()

    class Meta:
        verbose_name_plural = "Giorni"

    def __str__(self):
        return str(self.giorno) + " alle " + str(self.orario) 

class Film(models.Model):
    titolo = models.CharField(max_length=50)
    immagine = models.ImageField()
    regista = models.CharField(max_length=50)
    attore = models.CharField(max_length=50)
    sinossi = models.CharField(max_length=300)
    categoria = models.CharField(max_length=30)
    durata  = models.CharField(max_length=10)
    media_valutazione = models.FloatField(default=0,validators=[MaxValueValidator(10),MinValueValidator(0)])

    class Meta:
        verbose_name_plural="Film"
    
    def __str__(self):
        return self.titolo

class Sala(models.Model):
    s_immagine = models.ImageField(default=None)
    tipo = models.CharField(max_length=20)
    posti = models.IntegerField(default=20)
    giorno = models.OneToOneField(Giorno,on_delete=models.CASCADE,default="")
    film = models.ForeignKey(Film,on_delete=models.CASCADE,default="")

    class Meta:
        verbose_name_plural = "Sale"

    def __str__(self):
        return str(self.film) + " " + self.tipo + " " + str(self.giorno)


class Prenotazione(models.Model):
    user = models.ForeignKey(User,on_delete=models.PROTECT,default=None)
    sala = models.ForeignKey(Sala,on_delete=models.CASCADE,default="")
    n_posti = models.IntegerField(default=1,validators=[MaxValueValidator(20),MinValueValidator(1)])

    class Meta:
        verbose_name_plural = "Prenotazioni"

    def __str__(self):
        return "Prenotazione a nome " + str(self.user) + " per " + str(self.sala)

class Recensioni(models.Model):
    id = models.AutoField(primary_key=True)
    film = models.ForeignKey(Film, on_delete=models.CASCADE, default="")
    user = models.ForeignKey(User, on_delete=models.CASCADE, default="")
    recensione = models.CharField(max_length=300)
    stelle = models.FloatField(default=0, validators=[MaxValueValidator(10), MinValueValidator(1)])

    class Meta:
        verbose_name_plural = "Recensioni"

    def __str__(self):
        return str(self.film) + " voto: " + str(self.stelle)

class Attesa(models.Model):
    user = models.ForeignKey(User,on_delete=models.PROTECT,default=None)
    sala = models.ForeignKey(Sala,on_delete=models.CASCADE,default="")
    posti_richiesti = models.IntegerField(default=1,validators=[MaxValueValidator(20),MinValueValidator(1)])

    class Meta:
        verbose_name_plural = "Attesa"

    def __str__(self):
        return "Attesa posti liberi per" + str(self.user) + ": " + str(self.sala)
