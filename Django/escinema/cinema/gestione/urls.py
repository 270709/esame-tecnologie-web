from django.urls import path
from .views import *


app_name="gestione"

urlpatterns = [
    path('home/',listafilm,name="catalogo"),
    path('homesala/<int:id>',listasale,name="homesala"),
    path('creagiorno/',CreaGiornoView.as_view(),name="creagiorno"),
    path('storico/',situazionep,name="storico"),
    path('recensione/<int:id>',addRecensione, name="recensione"),
    path('vota/', vota, name="vota"),
    path('prenotazione/<int:id>',prenotazione,name="prenotazione"),
    path('modifica/<int:id>',cancella_p,name="modifica"),
    path('attesa/<int:id>',attesta_p,name="attesa"),
    path("inseriscifilm/", CreafilmView.as_view(), name="inseriscifilm"),
    path("inseriscisala/",CreaSalaView.as_view(),name="inseriscisala"),
    path("storicosala/",storicosala,name="storicosala"),
    path('rimuovi/<int:id>',rimuovisala,name="rimuovi"),
    path('storicofilm/', storicofilm, name="storicofilm"),
    path('rimuovifilm/<int:id>', rimuovifilm, name="rimuovifilm")

]