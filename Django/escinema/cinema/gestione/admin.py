import re
from django.contrib import admin

from gestione.models import Attesa, Film, Giorno, Prenotazione, Recensioni, Sala

# Register your models here.

admin.site.register(Giorno)
admin.site.register(Film)
admin.site.register(Sala)
admin.site.register(Prenotazione)
admin.site.register(Recensioni)
admin.site.register(Attesa)