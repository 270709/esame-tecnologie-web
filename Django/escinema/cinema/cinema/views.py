from django.shortcuts import render
from django.urls import reverse_lazy
from .forms import *
from gestione.models import *
from django.views.generic.edit import CreateView
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib import messages

def cinema_home(request):
    attesa= Attesa.objects.all()
    if request.user.groups.filter(name="staff").exists():
        c = True
    else:
        c = False
    return render(request, "home.html", {"listaattesa": attesa, 'staff': c})


class UserCreate(CreateView):
    form_class = CreaUtenteloggato
    template_name="create_user.html"
    success_url = reverse_lazy("login")

    def form_valid(self, form):
      messages.success(self.request, "utente creato correttamente")
      return super().form_valid(form)

class StaffCreate(PermissionRequiredMixin,UserCreate):
    permission_required = "is_staff"
    form_class= CreaUtenteStaff