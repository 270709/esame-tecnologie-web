Requisiti per il corretto funzionamento
	python version == 3.10.0 
	pipenv version == 2022.1.8 
	Django version == 4.0.5 
	django-crispy-forms version == 1.14.0 
	django-braces version == 1.15.0
	django-bootstrap-datepicker-plus version == 4.0.0
	Pillow version == 9.1.1