from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth.models import Group

class CreaUtenteloggato(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super(CreaUtenteloggato, self).__init__(*args, **kwargs)

        self.fields['email'].required = True

    def save(self, commit=True):
        user = super().save(commit) 
        g = Group.objects.get(name="logged") 
        g.user_set.add(user)
        return user

    class Meta:
        model = User
        fields =['username','email','password1','password2']
        



class CreaUtenteStaff(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super(CreaUtenteStaff, self).__init__(*args, **kwargs)

        self.fields['email'].required = True


        self.fields['email'].required = True

    def save(self,commit=True):
        user = super().save(commit)
        g = Group.objects.get(name="staff")
        g.user_set.add(user)
        return user

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']
