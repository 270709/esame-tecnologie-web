import datetime
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django.forms.widgets import *
from .models import *
from django.forms import ModelForm
from bootstrap_datepicker_plus.widgets import DatePickerInput

class SalaForm(ModelForm):
    helper = FormHelper()
    helper.form_id = "addfilm_crispy_form"
    helper.form_method = "POST"
    helper.add_input(Submit("submit", "Aggiungi film a Sala"))

    class Meta:
        model = Sala
        fields = ("tipo","s_immagine","posti","giorno","film")

class GiornoForm(ModelForm):
    helper = FormHelper()
    helper.form_id = "addorario_crispy_form"
    helper.form_method = "POST"
    helper.add_input(Submit("submit", "Aggiungi Giorno e orario"))

    class Meta:
        model = Giorno
        fields = ("giorno", "orario")
        widgets = {
            'giorno' : DatePickerInput (
                options={
                    'minDate' :(datetime.datetime.today() + datetime.timedelta(days=1)).strftime('%Y-%m-%d '),
                    'maxDate': (datetime.datetime.today() + datetime.timedelta(days=15)).strftime('%Y-%m-%d ')
                }
            )
        }


class CreafilmForm(ModelForm):
    helper = FormHelper()
    helper.form_id = "addfilm_crispy_form"
    helper.form_method = "POST"
    helper.add_input(Submit("submit", "Aggiungi Film"))

    class Meta:
        model = Film
        fields = ("titolo", "immagine", "regista", "attore", "sinossi", "durata", "categoria")


class RecensioniForm(ModelForm):
    helper = FormHelper()
    helper.form_show_labels = False

    class Meta:
        model = Recensioni
        fields = ['recensione', 'stelle']


class PrenForm(ModelForm):
    helper = FormHelper()
    helper.form_show_labels = False
    helper.add_input(Submit("submit", "Prenotati"))

    class Meta:
        model = Prenotazione
        fields = ['n_posti']


class AttesaForm(ModelForm):
    helper = FormHelper()
    helper.form_show_labels = False
    helper.add_input(Submit("submit", "Mettiti in attesa"))

    class Meta:
        model = Attesa
        fields = ['posti_richiesti']
