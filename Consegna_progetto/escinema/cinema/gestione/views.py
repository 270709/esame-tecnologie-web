from django.contrib import messages
from .models import *
from .forms import *
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView
from django.contrib.auth.decorators import login_required
from braces.views import GroupRequiredMixin
from .models import *


def listafilm(request):

    listaidfilmprenotati = []  # lista film prenotati
    listaprenotazioni = []  # lista id user che hanno fatto prenotazioni
    listacategoriafilmprenotati = []  # lista dei generi dei film da cercare
    listaregisti = []  # lista registi dei film prenotati già
    listaattori = []  # lista attori dei film prenotati
    listaspettacoli = []  # lista contenente tutti gli spettacoli del cinema

    film_cons_attori=[] #film consigliati per attore
    film_cons_genere=[] #film consigliati per categoria
    film_cons_regista=[] #film consigliati per registi

    spettacoli = Sala.objects.all()
    listafilm = Film.objects.order_by('-media_valutazione')
    pren = Prenotazione.objects.all()

    if request.user.is_authenticated:
        user = User.objects.get(pk=request.user.id)
        for s in spettacoli:
            listaspettacoli.append(s.film_id)

        for p in pren:
            if user.id == p.user_id:
                listaidfilmprenotati.append(p.sala.film_id)

                listaprenotazioni.append(p.user_id)

                listacategoriafilmprenotati.append(p.sala.film.categoria)

                listaregisti.append(p.sala.film.regista)

                listaattori.append(p.sala.film.attore)

        for l in listafilm:
            if (l.categoria in listacategoriafilmprenotati and l.id not in listaidfilmprenotati and l.id in listaspettacoli):
                film_cons_genere.append(l)
            if (l.regista in listaregisti and l.id not in listaidfilmprenotati and l.id in listaspettacoli):
                film_cons_regista.append(l)
            if (l.attore in listaattori and l.id not in listaidfilmprenotati and l.id in listaspettacoli):
                film_cons_attori.append(l)

        ctx= {'listafilm': listafilm, 'listapren': listaprenotazioni, 'listafilmprenotati': listaidfilmprenotati,
                   'listaspettacoli': listaspettacoli, 'listagenere': listacategoriafilmprenotati,
                   'listaregisti': listaregisti, 'listaattori': listaattori,'cons_regista':film_cons_regista,'cons_attore':film_cons_attori,'cons_genere':film_cons_genere}
    else:
        ctx={"listafilm":listafilm}

    return render(request, 'gestione/home.html',ctx)



def listasale(request, id):
    listasale = Sala.objects.order_by('posti')
    return render(request, 'gestione/homesala.html', {'listasale': listasale})

@login_required
def situazionep(request):
    user = get_object_or_404(User, pk=request.user.pk)
    # qui ho creato una lista dove vengono inserite le prenotazioni di ogni user

    storico = []
    # quando provi ad entrare questo ciclo inserisce tutte le tue prenotazioni in pren che poi verrà usato dall'html in seguito

    for p in Prenotazione.objects.all():
        # controllo se quell'user ha effettuato prenotazioni
        if user.id == p.user_id:

            for s in Sala.objects.all():
                if s.id == p.sala_id:
                    # aggiungo prenotazione alla lista
                    storico.append(p)
    ctx = {'listafilm': storico}
    return render(request, "gestione/storico.html", ctx)


@login_required
def addRecensione(request, id):
    form = RecensioniForm()
    fi = Film.objects.get(pk=id)
    user = User.objects.get(pk=request.user.id)

    if request.method == 'POST':
        form = RecensioniForm(request.POST)
        if form.is_valid():
            c = form.save(commit=False)
            c.film = fi
            c.user = user
            c.save()
            s_recensioni = Recensioni.objects.filter(film=fi)
            somma = fi.media_valutazione + c.stelle
            if (len(s_recensioni) <= 2):
                fi.media_valutazione = somma / len(s_recensioni)
            else:
                fi.media_valutazione = somma / 2
            fi.media_valutazione = round(fi.media_valutazione, 1)
            fi.save()

            messages.success(request,"recensione avvenuta correttamente")
            return redirect('home')
    return render(request, 'gestione/recensioni.html', {"form": form})


def vota(request):
    user = get_object_or_404(User, pk=request.user.pk)
    pren = []
    for p in Prenotazione.objects.all():
        if user.id == p.user_id:
            for s in Sala.objects.all():
                if s.id == p.sala_id:
                    pren.append(s)
    ctx = {'listafilm': pren}
    return render(request, "gestione/vota.html", ctx)





def prenotazione(request, id):
    form = PrenForm()
    sala = Sala.objects.get(id=id)
    att = Attesa.objects.all()
    if request.method == 'POST':
        form = PrenForm(request.POST)
        sala = Sala.objects.get(pk=id)
        us_pren = User.objects.get(pk=request.user.id)
        for t in att:
            if (t.user_id == us_pren.id) and (t.sala_id == sala.id):
                r = Attesa.objects.get(pk=t.id)
                r.delete()
        if form.is_valid():
            c = form.save(commit=False)
            if c.n_posti > sala.posti:
                messages.info(request, "posti non disponibili")
            else:
                c.user = us_pren
                c.sala = sala
                sala.posti -= c.n_posti
                sala.save()
                c.save()
                messages.success(request,"prenotazione avvenuta con successo")
                return redirect('gestione:catalogo')
    return render(request, "gestione/pren.html", {"sala": sala, "form": form, "film": listafilm})


@login_required
def cancella_p(request, id):
    pren = Prenotazione.objects.get(pk=id)
    nposti = pren.n_posti
    sa = pren.sala_id
    pren.delete()
    s = Sala.objects.get(pk=sa)
    s.posti += nposti
    s.save()
    messages.success(request,"cancellazione prenotazione con successo")
    return redirect("http://127.0.0.1:8000/gestione/home/")


def attesta_p(request, id):
    form = AttesaForm()
    sala = Sala.objects.get(id=id)
    if request.method == 'POST':
        form = AttesaForm(request.POST)
        sala = Sala.objects.get(pk=id)
        us_pren = User.objects.get(pk=request.user.id)
        if form.is_valid():
            c = form.save(commit=False)
            c.user = us_pren
            c.sala = sala
            c.save()
            messages.success(request,"inserimento in lista avvenuto correttamente")
            return redirect('gestione:catalogo')
    return render(request, "gestione/pren.html", {"sala": sala, "form": form})

#sezione creazione spettacolo

class CreafilmView(GroupRequiredMixin, CreateView):
    group_required = ["staff"]
    title = "Aggiungi film al catalogo"
    form_class = CreafilmForm
    template_name = "gestione/crea_film.html"
    success_url = reverse_lazy("gestione:inseriscisala")

    def form_valid(self, form):
      messages.success(self.request, "film creato")
      return super().form_valid(form)

class CreaGiornoView(GroupRequiredMixin, CreateView):
    group_required = ["staff"]
    title = "Aggiunta di un orario settimanale"
    form_class = GiornoForm
    template_name = "gestione/crea_ora.html"
    success_url = reverse_lazy("gestione:inseriscisala")

    def form_valid(self, form):
      messages.success(self.request, "giorno creato")
      return super().form_valid(form)



class CreaSalaView(GroupRequiredMixin, CreateView):
    group_required = ["staff"]
    title = "Aggiungi Sala"
    form_class = SalaForm
    template_name = "gestione/crea_sala.html"
    success_url =  reverse_lazy("home")

    def form_valid(self, form):
      messages.success(self.request, "spettacolo creato correttamente")
      return super().form_valid(form)


@login_required
def storicosala(request):
    s = Sala.objects.all()
    return render(request, 'gestione/storico_sala.html', {'listasale': s})

@login_required
def rimuovisala(request,id):
    pren = Sala.objects.get(pk=id)
    pren.delete()
    messages.success(request,"sala rimossa correttamente")
    return redirect('home')

def storicofilm(request):
    f = Film.objects.all()
    return render(request,'gestione/cancellazione_film.html',{'listafilm' : f})

@login_required
def rimuovifilm(request,id):
    fi = Film.objects.get(pk=id)
    fi.delete()
    messages.success(request,"film rimosso correttamente")
    return redirect('home')


