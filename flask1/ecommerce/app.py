from sqlalchemy import desc
import os
from flask import Flask, render_template, url_for,send_from_directory,redirect,request,flash
from flask_sqlalchemy import SQLAlchemy
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from flask_migrate import Migrate
from flask_login import LoginManager,login_user,login_manager,login_required,logout_user,current_user
from werkzeug.security import generate_password_hash, check_password_hash
from config import Config
from forms import *
from datetime import date
from flask_uploads import configure_uploads, IMAGES, UploadSet


app = Flask (__name__)


from functools import wraps
import time

def timer(func):
    """helper function to estimate view execution time"""

    @wraps(func)  # used for copying func metadata
    def wrapper(*args, **kwargs):
        # record start time
        start = time.time()

        # func execution
        result = func(*args, **kwargs)
        
        duration = (time.time() - start) * 1000
        # output execution time to console
        print('view {} takes {:.2f} ms'.format(
            func.__name__, 
            duration
            ))
        return result
    return wrapper



app.config.from_object(Config)
app.config['SECRET_KEY'] = Config.SECRET_KEY
app.config['FLASK_ADMIN_SWATCH'] = 'cerulean'
app.config['UPLOADED_IMAGES_DEST']='static/imgs'
admin = Admin(app)

def create_app():
    app = Flask(__name__)
    app.config['TESTING'] = True
    app.config['WTF_CSRF_ENABLED'] = False
    db.init_app(app)
    return app

images=UploadSet('images',IMAGES)
configure_uploads(app,images)

class AdminView(ModelView):
    def is_accessible(self):
        if current_user.id==1:
            return True
    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('homepage',next=request.url))

db = SQLAlchemy(app)
migrate = Migrate(app, db)
from models import *



login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'



admin.add_view(AdminView(User,db.session))
admin.add_view(AdminView(Prodotto,db.session))
admin.add_view(AdminView(Carrello,db.session))
admin.add_view(AdminView(Storico,db.session))
admin.add_view(AdminView(Recensioni,db.session))

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

@app.route("/static/<path:path>")
def static_dir(path):
    return send_from_directory("static", path)

@app.route("/home/",methods=["GET","POST"])
@timer
def homepage():
    return render_template('index.html',title='home')

@app.route("/registrati/",methods=["GET","POST"])
def registrati():
    form = RegistrationForm()
    if form.validate_on_submit():
        if request.method=='POST':
            username1 = request.form['username']
            name1 = request.form['name']
            surname1 = request.form['surname']
            email1 = request.form['email']
            password_new = request.form['password']
            hashed_pw = generate_password_hash(password_new)
            new_user = User(username=username1,name=name1,surname=surname1,email=email1,password_hash=hashed_pw,authenticated=True)
            try:
                db.session.add(new_user)
                db.session.commit()
                return redirect(url_for('homepage'))
            except: 
                return "errore nella creazione utente \n"
    return render_template('create_user.html',title='registrati',form=form)

@app.route('/login/',methods=["GET","POST"])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        att = form.username.data
        u = User.query.filter_by(username=att).first()
        if u:
            if check_password_hash(u.password_hash,form.password.data):
                login_user(u)
                return redirect(url_for('homepage'))
            else:
                flash("wrong password - try again!!")
        else:
            flash("user doesnt exist - try again!!")
    return render_template('login.html',form=form)



@app.route('/dashboard/',methods=["GET","POST"])
@login_required
def dashboard():
    return render_template('dashboard.html')

@app.route('/logout/',methods=["GET","POST"])
@login_required
def logout():
    logout_user()
    return redirect(url_for('homepage'))

@app.route('/addprod/',methods=["GET","POST"])
@login_required
def add_prod():
    if current_user.id != 1:
        return redirect(url_for('homepage'))
    form = ProdottoForm()
    if form.validate_on_submit():
        nome1=form.nome.data
        image = images.save(form.immagine.data)
        costo = form.prezzo.data
        cat = form.categoria.data
        qta = form.qta_disponibile.data
        media = form.media_valutazione.data
        desc = form.descrizione.data

        new_prod=Prodotto(nome=nome1,immagine=image,prezzo=costo,categoria=cat,qta_disponibile=qta,media_valutazione=media,descrizione=desc)
        try:
            db.session.add(new_prod)
            db.session.commit()
            return redirect(url_for('homepage'))
        except:
            return "errore nell'aggiunta del prodotto"
    return render_template('add_prodotto.html',form=form)



@app.route('/prodotti/',methods=['GET','POST'])
def prodotti():
    listap = Prodotto.query.order_by(desc(Prodotto.media_valutazione))
    return render_template('lista_prodotti.html',listap=listap)


@app.route('/carrello/<int:id>',methods=["GET","POST"])
@login_required
def Acarrello(id):
    form = CarrelloForm()
    id_utente = current_user.id 
    prodotto_car = Prodotto.query.filter_by(id_prodotto=id).first()
    if form.validate_on_submit():
        qta = form.qta_richiesta.data
        if qta > prodotto_car.qta_disponibile:
            flash("quantità non disponibile")
            return redirect(url_for('homepage'))
        else: 
            new_carrello = Carrello(user_id=id_utente,prodotto_id=prodotto_car.id_prodotto,qta_richiesta=qta)
            prodotto_car.qta_disponibile -= qta
            try:
                db.session.add(new_carrello)
                db.session.commit()
                return redirect(url_for('homepage'))
            except: 
                return "Error"
    return render_template('carrello.html',form=form, prodotto_car=prodotto_car)


@app.route('/prenotazione/',methods=['GET','POST'])
@login_required
def prenotazione():
    id = current_user.id 
    carrello =[]
    car = Carrello.query.all()
    costo = 0
    ap = 0
    for c in car:
        if c.user_id ==id:
            carrello.append(c)
            ap =c.prodotto.prezzo * c.qta_richiesta
            costo +=ap
    return render_template('prenotazione.html',carrello=carrello,costo=costo,car=car)

@app.route('/acquisto/',methods=['GET','POST'])
@login_required
def acquisto():
    car = Carrello.query.all()
    car_user=[]
    for c in car:
        if c.user_id == current_user.id:
            car_user.append(c)
            try:
                db.session.delete(c)
                db.session.commit()
            except:
                return "errore  nella cancellazione del prodotto"
    today= date.today()
    for a in car_user:
        acq = Storico(user_id=a.user_id,prodotto_id=a.prodotto_id,qta=a.qta_richiesta,data=today)
        try:
            db.session.add(acq)
            db.session.commit()
        except:
            return "errore nell'aggiunta del prodotto"
    return redirect(url_for('homepage'))

@app.route('/storico/',methods=['GET','POST'])
@login_required
def storico():
    acquisti = Storico.query.all()
    ac_user=[]
    for c in acquisti:
        if current_user.id == c.user_id:
            ac_user.append(c)
    return render_template('storico.html',ac_user=ac_user)

@app.route('/valutazione/<int:id>',methods=['GET','POST'])
@login_required
def valutazione(id):
    form = RecensioniForm()
    s_prodotto = Prodotto.query.filter_by(id_prodotto=id).first()
    user = User.query.filter_by(id=current_user.id).first()

    if form.validate_on_submit():
        n_rec = Recensioni.query.filter_by(prodotto_id=s_prodotto.id_prodotto).count()
        storico =Recensioni.query.filter_by(prodotto_id=s_prodotto.id_prodotto)
        somma = 0
        somma += form.voto.data
        for s in storico:
            somma += s.voto
        media_voto = somma / (n_rec + 1)
        s_prodotto.media_valutazione = round(media_voto,1)
        new_recensione=Recensioni(user_id=user.id,prodotto_id=s_prodotto.id_prodotto,voto=form.voto.data)
        try:
            db.session.add(new_recensione)
            db.session.commit()
        except:
            return "errore nella creazione recensione\n"
        return redirect(url_for('homepage'))
    return render_template('recensione.html',form=form)

@app.route('/about/',methods=['GET','POST'])
@timer
def AboutUs():
    return render_template('about.html')


@app.route('/modifica/<int:id>',methods=['GET','POST'])
@login_required
def modificaC(id):
    form = ModificaForm()
    if form.validate_on_submit():
        car=Carrello.query.filter_by(id_carrello=id).first()
        if current_user.id == car.user_id:
            prod = Prodotto.query.filter_by(id_prodotto=car.prodotto_id).first()
            qta = form.qta_modifica.data - car.qta_richiesta
            if prod.qta_disponibile < qta:
                return "errore qta non disponibile"
            else:
                ap=car.qta_richiesta
                car.qta_richiesta=form.qta_modifica.data
                ap = form.qta_modifica.data - ap
                prod.qta_disponibile-=ap
                db.session.commit()
                return redirect(url_for('prenotazione'))
        else:
            flash("non è la tua prenotazione")
    return render_template('modifica.html',form=form)

@app.route('/cancella/<int:id>',methods=['GET','POST'])
@login_required
def cancella(id):
    car = Carrello.query.filter_by(id_carrello=id).first()
    if car.user_id == current_user.id:
        qta = car.qta_richiesta
        prod = Prodotto.query.filter_by(id_prodotto=car.prodotto_id).first()
        prod.qta_disponibile += qta
        try:
            db.session.delete(car)
            db.session.commit()
        except:
            return "errore  nella cancellazione del carrello"
    return redirect(url_for('prenotazione'))
