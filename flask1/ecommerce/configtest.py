
from flask_login import current_user
from app import db,app
import unittest
from flask import Flask
from app import create_app
from models import User


class TestLoginView(unittest.TestCase):

    def test_login_successo(self):
        tester = app.test_client(self)
        with tester:
            response = tester.post('/login/',data=dict(username='admin',password_hash='danilo22'),follow_redirects=True)
            self.assertEqual(response.status_code,200)

    def test_login_nome_errato(self):
        tester = app.test_client(self)
        response = tester.post('/login/',data=dict(username='rest',password_hash='danilo22'))
        self.assertIn(b'Pagina di Login',response.data)

    def test_login_password_errato(self):
        tester = app.test_client(self)
        response = tester.post('/login/',data=dict(username='admin',password_hash='rest4'),follow_redirects=False)
        self.assertTrue(response.status_code==200)


        
    def test_login_username_e_password_errati(self):
        tester=app.test_client(self)
        response = tester.post('/login/',data=dict(username="ripd",password_hash="123456"),follow_redirects=False)
        self.assertEqual(response.status_code,200)
        self.assertIn(b'Pagina di Login',response.data)




if __name__ == '__main__':
    unittest.main()






