# Generated by Django 4.1.1 on 2022-09-27 09:39

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('prodotti', '0002_remove_storico_ac_prodotto_storico_ac_prodotti_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='carrello',
            name='qta_richiesta',
            field=models.IntegerField(default=0, validators=[django.core.validators.MaxValueValidator(50), django.core.validators.MinValueValidator(1)]),
        ),
        migrations.AddField(
            model_name='prodotto',
            name='qta_disponibile',
            field=models.IntegerField(default=0),
        ),
    ]
