from django.contrib.auth.models import User
from django.forms import *
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from djmoney.models.fields import MoneyField

# Create your models here.

CATEGORIE = [
    ("Frutta","Frutta"),
    ("Verdura","Verdura"),
    ("Bulbi","Bulbi"),
    ("Animali","Animali"),
    ("Fitosanitari","Fitosanitari"),
]

class Prodotto(models.Model):
    id_prodotto = models.AutoField(primary_key=True)
    nome = models.CharField(max_length=50)
    immagine = models.ImageField()
    prezzo = MoneyField(decimal_places=2,default=0,default_currency='EUR',max_digits=10)
    categoria= models.CharField(max_length=13,choices=CATEGORIE,default="Frutta")
    qta_disponibile = models.IntegerField(default=0)
    media_valutazione = models.FloatField(default=0,validators=[MaxValueValidator(10),MinValueValidator(0)])
    descrizione = models.CharField(max_length=300)

    class Meta:
        verbose_name_plural = "Prodotti"

    def __str__(self):
        return self.nome

class Carrello(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE,default="")
    prodotto = models.ForeignKey(Prodotto,on_delete=models.CASCADE,default="")
    qta_richiesta = models.IntegerField(default=0,validators=[MaxValueValidator(50),MinValueValidator(1)])

    class Meta:
        verbose_name_plural = "Carrello"

    def __str__(self):
        return str(self.user) + " " + str(self.prodotto)

class Storico_Ac(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE,default="")
    prodotti = models.ForeignKey(Prodotto,on_delete=models.CASCADE,default="")
    qta = models.IntegerField(default=0)
    data = models.DateField()

    class Meta:
        verbose_name_plural = "Storico Acquisti"

    def __str__(self):
        return str(self.user) + "in data " + str(self.data) +" " + "ha acquistato " +  str(self.prodotti)

class Valutazione(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User,on_delete=models.CASCADE,default="")
    prodotto = models.ForeignKey(Prodotto,on_delete=models.CASCADE,default="")
    voto = models.FloatField(default=1,validators=[MaxValueValidator(10),MinValueValidator(1)])

    class Meta:
        verbose_name_plural = "Recensioni"

    def __str__(self):
        return str(self.prodotto) + "Voto: " + str(self.voto)
