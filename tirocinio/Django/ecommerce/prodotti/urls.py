from django.urls import path
from .views import *

app_name="prodotti"

urlpatterns = [
    path('catalogo/',listaprodotti,name='catalogo'),
    path('carrello/<int:id>',agg_carrello,name='carrello'),
    path('prenotazione/',prenotazione,name='prenotazione'),
    path('acquisto/',acquisto,name='acquisto'),
    path('storico/',storico,name='storico'),
    path('valutazione/<int:id>',recensione,name='recensione'),
    path('modifica/<int:id>',modificaOrdine,name='modifica'),
    path('cancella/<int:id>',cancellaOrdine,name='cancella'),
]