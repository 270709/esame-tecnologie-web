from pyexpat import model
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django.forms.widgets import *
from .models import *
from django.forms import ModelForm

class AggiuntaCarrelloForm(ModelForm):
    helper = FormHelper()
    helper.form_show_labels = False
    helper.add_input(Submit("submit","Aggiungi al carrello"))

    class Meta:
        model= Carrello
        fields = ['qta_richiesta']

class RecensioneForm(ModelForm):
    helper = FormHelper()
    helper.form_show_labels = False
    helper.add_input(Submit("submit","Effettua la votazione"))

    class Meta:
        model=Valutazione
        fields = ['voto']

class ModificaForm(ModelForm):
    helper = FormHelper()
    helper.form_show_labels = False
    helper.add_input(Submit("submit","Effettua Modifica"))

    class Meta:
        model= Carrello
        fields = ['qta_richiesta']