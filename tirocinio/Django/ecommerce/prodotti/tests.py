from django.urls import reverse
from django.test import TestCase, Client
from .models import *

# Create your tests here.




class LoginTestVieW(TestCase):
    def setUp(self):
        self.client = Client()
        self.user1 = {
            'username' : 'user1',
            'email' : 'user@prova.com',
            'password' : '12345'
        }
        User.objects.create_user(**self.user1)
        self.vota_url = reverse('login')


    def test_login_corretto(self):
        response = self.client.post(self.vota_url,self.user1, follow=True)
        self.assertTrue(response.context['user'].is_authenticated)

    def test_login_password_errata(self):
        response = self.client.post(self.vota_url,{
            'username' : 'user1',
            'password' : '444444'
        },follow=True)
        self.assertFalse(response.context['user'].is_authenticated)

    def test_login_username_errato(self):
        response = self.client.post(self.vota_url,{
            'username' : 'user2',
            'password' : '12345'
        },follow=True)
        self.assertFalse(response.context['user'].is_authenticated)

    def test_login_username_e_password_errato(self):
        response = self.client.post(self.vota_url,{
            'username' : 'user2',
            'password' : 'provapassword'
        },follow=True)
        self.assertFalse(response.context['user'].is_authenticated)






class VotaViewTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.prodotto1 = Prodotto.objects.create(
            nome = 'prodotto',
            immagine = 'foto.jpg',
            prezzo = '2.50',
            categoria = 'Verdura',
            qta_disponibile = '10',
            media_valutazione = '0',
            descrizione = 'prodotto con descrizione'
            )

        self.user1 = User.objects.create(
            username = 'user1',
            email = 'user@prova.com'
        )
        self.user1.set_password('12345')
        self.user1.save()
        self.logged_in = self.client.login(username='user1',password='12345')

        self.vota_url = reverse('prodotti:recensione',args =[self.prodotto1.id_prodotto])


    def test_recensione_corretta_POST_succes_redirect(self):
        '''
            test che si occupa di verificare che dato l'url delle recensioni effettua una richiesta
            POST con i dati relativi alla recensione di un prodotto e che poi si venga redirectati alla
            home
        '''
        response = self.client.post(self.vota_url, {
            'prodotto' : self.prodotto1.id_prodotto,
            'user' : self.user1.id,
            'voto' : '7'
        }, follow=True)

        self.assertRedirects(response,reverse('home'), status_code=302, target_status_code=200)
        self.assertTemplateUsed(response,'home.html')

    def test_recensione_non_corretta_voto_mancante_POST_not_success_redirect(self):
        '''
            test che si occupa di verificare che dato l'url delle recensioni effettua una richiesta
            POST con il dato numerico (voto) mancante e quindi non si viene redirectati e si 
            visualliza la stessa pagina con l'errore
        '''

        response = self.client.post(self.vota_url, {
            'prodotto' : self.prodotto1.id_prodotto,
            'user' : self.user1.id,
        }, follow=True)

        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response,'prodotti/votazione.html')


    def test_recensione_voto_maggiore_10_POST_not_success_redirect(self):
        '''
            test che si occupa di verificare che dato l'url delle recensioni effettua una richiesta
            POST con il dato numerico (voto) maggiore di 10 quindi non si viene redirectati e si 
            visualliza la stessa pagina con l'errore
        '''

        response = self.client.post(self.vota_url, {
            'prodotto' : self.prodotto1.id_prodotto,
            'user' : self.user1.id,
            'voto' : '15'
        }, follow=True)

        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response,'prodotti/votazione.html')


    def test_recensione_voto_ugale_a_0_POST_not_success_redirect(self):
        '''
            test che si occupa di verificare che dato l'url delle recensioni effettua una richiesta
            POST con il dato numerico (voto) uguale a 0 quindi non si viene redirectati e si 
            visualliza la stessa pagina con l'errore
        '''

        response = self.client.post(self.vota_url, {
            'prodotto' : self.prodotto1.id_prodotto,
            'user' : self.user1.id,
            'voto' : '0'
        }, follow=True)

        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response,'prodotti/votazione.html')


    def test_recensione_voto_minore_di_0_POST_not_success_redirect(self):
        '''
            test che si occupa di verificare che dato l'url delle recensioni effettua una richiesta
            POST con il dato numerico (voto) minore di 0 quindi non si viene redirectati e si 
            visualliza la stessa pagina con l'errore
        '''

        response = self.client.post(self.vota_url, {
            'prodotto' : self.prodotto1.id_prodotto,
            'user' : self.user1.id,
            'voto' : '-3'
        }, follow=True)

        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response,'prodotti/votazione.html')


    def test_recensione_voto_carattere_non_numerico_POST_not_success_redirect(self):
        '''
            test che si occupa di verificare che dato l'url delle recensioni effettua una richiesta
            POST con il dato (voto) non numerico ma espresso con caratteri quindi non si viene 
            redirectati e si visualliza la stessa pagina con l'errore
        '''

        response = self.client.post(self.vota_url, {
            'prodotto' : self.prodotto1.id_prodotto,
            'user' : self.user1.id,
            'voto' : 'sette'
        }, follow=True)

        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response,'prodotti/votazione.html')


        


    
