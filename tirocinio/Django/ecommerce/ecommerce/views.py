from django.shortcuts import render
from django.urls import reverse_lazy
from .forms import *
from django.views.generic.edit import CreateView
from django.contrib import messages
from functools import wraps
import time

def timer(func):
    """helper function to estimate view execution time"""

    @wraps(func)  # used for copying func metadata
    def wrapper(*args, **kwargs):
        # record start time
        start = time.time()

        # func execution
        result = func(*args, **kwargs)
        
        duration = (time.time() - start) * 1000
        # output execution time to console
        print('view {} takes {:.2f} ms'.format(
            func.__name__, 
            duration
            ))
        return result
    return wrapper


@timer
def ecommerce_home(request):
    return render(request,"home.html")

class UserCreate(CreateView):
    form_class = CreaUtentelog
    template_name="create_user.html"
    success_url = reverse_lazy('login')

    def form_valid(self, form):
      messages.success(self.request, "utente creato correttamente")
      return super().form_valid(form)
@timer
def AboutUs(request):
    return render(request,'about.html')


