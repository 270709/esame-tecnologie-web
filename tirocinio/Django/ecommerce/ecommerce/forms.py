from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth.models import Group

class CreaUtentelog(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super(CreaUtentelog, self).__init__(*args, **kwargs)

        self.fields['email'].required = True
        self.fields['first_name'].required=True
        self.fields['last_name'].required=True

    def save(self, commit=True):
        user = super().save(commit) 
        g = Group.objects.get(name="logged") 
        g.user_set.add(user)
        return user

    class Meta:
        model = User
        fields =['username','first_name','last_name','email','password1','password2']

